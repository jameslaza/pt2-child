<?php
/**
 * Template Name: Testimonial Template
 *
 * @package WordPress
 * @subpackage SWM
 * @since SWM 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		
		<?php echo swm_innerpage_banner_modules(); ?>
		<?php swm_breadcrumbs();?>
		
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<?php get_template_part('partials/content-page'); ?>
		<?php endwhile; endif; // close the WordPress loop  ?>

		<?php get_template_part('partials/section','testimonial-list'); ?>

		<?php 
			get_template_part('partials/generic/pas');
			get_template_part('partials/generic/rate', 'us');
			get_template_part('partials/generic/info', 'bar');
		?>		

	</div><!-- #primary -->

<?php
get_footer();
