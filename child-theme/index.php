<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package swm
 */

get_header(); ?>

	<div id="primary" class="content-area blog-container">

		<?php swm_breadcrumbs(); ?>

		<main id="main" class="site-main generic-main-content" role="main">

		<div class="container">
			<div class="row">
				<div class="col-md-12 content listing">
					<?php

					if ( have_posts() ) :

						if ( is_home() && ! is_front_page() ) : ?>
						<?php 
							$blogid = get_option( 'page_for_posts' );
	           				$blogname = get_the_title($blogid);
						?>
							<header class="entry-header has-subtitle">
								<h1 class="page-title"><?php echo $blogname; ?></h1>
							</header>

						<?php
						echo get_field('before_list_post',$blogid);
						endif;

						/* Start the Loop */
						while ( have_posts() ) : the_post();

							/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							get_template_part( 'partials/content', get_post_format() );

						endwhile;

						echo get_field('before_list_post',$blogid);

						// Previous/next page navigation.
						the_posts_pagination( array(
							'prev_text'          => __( '<', 'swm' ),
							'next_text'          => __( '>', 'swm' ),
							'screen_reader_text' => __( ' ' )
						) );

					else :

						get_template_part( 'partials/content', 'none' );

					endif; ?>

				</div>
			</div>
		</div>
		
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_template_part('partials/section', 'static-modules'); ?>
<?php

echo footer_section_blog_list();
get_footer();
