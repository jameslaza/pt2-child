jQuery(function($) {
//jQuery(document).ready(function($) {

jQuery( window ).scroll(function() {

    if (jQuery(window).width() > 768) {
      var header_container = jQuery('.site-header'),
        socialmedia_icons = header_container.find('.social-icon-container'),
        address_container = header_container.find('.contact-details address');

        var windowTop = jQuery(window).scrollTop();
        if ( windowTop < 80 ) {
              jQuery(document.body).trigger("sticky_kit:recalc");
              socialmedia_icons.show();
              //address_container.show();
              jQuery('body').removeClass('sticky-header-activated');       
        }else{
              jQuery('body').addClass('sticky-header-activated');
              socialmedia_icons.hide();
              //address_container.hide();
        }
    }else{
      var header_container = jQuery('.site-header');
      jQuery('body').removeClass('sticky-header-activated');
    }
});


/*jQuery( window ).scroll(function() {
    if (jQuery(window).width() > 768) {
  setTimeout(
    function() 
    {

    jQuery('.layout-header-c-wrapper .header-matchheight').matchHeight({
        byRow: true,
        property: 'height',
        target: null,
        remove: false
    });
    jQuery('.layout-header-b-wrapper .header-matchheight').matchHeight({
        byRow: true,
        property: 'height',
        target: null,
        remove: false
    });

    }, 200);
    }
});*/


  setChild_height("ul.faq-list");
  jQuery( window ).on("resize", function() {
    setChild_height("ul.faq-list");

      if (jQuery(window).width() > 1024) {
      jQuery(function() {
        var header_container = jQuery('.site-header');
        
        return header_container.stick_in_parent().delay(800).on("sticky_kit:stick", (function(_this) {
          return function(e) {
            return setTimeout(function() {}, 0);
          };
        })(this));
      });
    }

  }).resize();



function setChild_height(container){
  var pMaxHeight = -1;
  if (jQuery(window).width() > viewport_width()) {
    jQuery(container).each(function(){
          // this is inner scope, in reference to the .phrase element
          $header = jQuery('.site-header');
          $header = $header.height() - 34;
          //jQuery(this).find('li.faq-count').css({'margin-top':'-'+$header+'px', 'float':'left'});
          jQuery(this).find('li.faq-count').each(function(){
            jQuery(this).find('a.anchor').css('top','-'+$header+'px');
          });
    });
  }
}

  // This will select everything with the class smoothScroll
  // This should prevent problems with carousel, scrollspy, etc...
  jQuery('.smoothScroll').click(function () {

    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = jQuery(this.hash);
      target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
      if (target.length) {

        jQuery('html,body').animate({
          scrollTop: target.offset().top
        }, 800); // The number here represents the speed of the scroll in milliseconds
        return false;
      }
    }
  });

});

//set width based on header layout
function viewport_width(){
  var layout_header_b = jQuery('body').hasClass('layout-header-b');
  var layout_header_c = jQuery('body').hasClass('layout-header-c');
    var viewport_width = (layout_header_b==true || layout_header_c==true)? 100 : 1024;
    return viewport_width;
}


(function() {
  if (jQuery(window).width() > viewport_width()) {
  jQuery(function() {
    var header_container = jQuery('.site-header');

    return header_container.stick_in_parent().on("sticky_kit:stick", (function(_this) {
      return function(e) {
        return setTimeout(function() {}, 0);
      };
    })(this));
  });
}

}).call(this);