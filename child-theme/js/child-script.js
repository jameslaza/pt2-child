jQuery(function($) {
    // Load
    function external_links_in_new_windows_load(func){   
        var oldonload = window.onload;
        if (typeof window.onload != 'function'){
            window.onload = func;
        } else {
            window.onload = function(){
                oldonload();
                func();
            }
        }
    }
    external_links_in_new_windows_load(external_links_in_new_windows_loop);

    jQuery('.generic-content-c .generic-content-c-matchHeight').matchHeight({
        byRow: true,
        property: 'height',
        target: null,
        remove: false
    });

    jQuery('.banner-1-matchHeight').matchHeight({
        byRow: true,
        property: 'height',
        target: null,
        remove: false
    });

    content_height('.doctor-overview .item-content', '.doctor-overview .item-content-container', 768);

    jQuery(window).on("resize", function() {
        content_height('.doctor-overview .item-content', '.doctor-overview .item-content-container', 768);
    }).resize();


    /*--------------------------------------------------------------
    # Equal Height
    --------------------------------------------------------------*/
    function content_height(old_container, new_container, ScreenWidth) {
        var pMaxHeight = -1;
        var node;
        jQuery(old_container).each(function(index) {
            if (jQuery(this).outerHeight() > pMaxHeight) {
                pMaxHeight = jQuery(this).outerHeight();
                node = index;
            }
        });
        var _text_height = pMaxHeight;
        var _text_wrapper = jQuery(new_container);
        _text_wrapper.css('height', _text_height);
        jQuery(window).resize(function() {
            var pMaxHeight = -1;
            var node;
            jQuery(old_container).each(function(index) {
                if (jQuery(this).outerHeight() > pMaxHeight) {
                    pMaxHeight = jQuery(this).outerHeight();
                    node = index;
                }
            });
            var _text_height = pMaxHeight;
            var _text_wrapper = jQuery(new_container);
            var w = jQuery(window).width();

            if (w > ScreenWidth) {
                _text_wrapper.css('height', _text_height);
            } else {
                _text_wrapper.css('height', 'auto');
            }
            //console.log(_text_wrapper);
        });
    }

    jQuery(".ebook-input [name='firstname']").attr('required', 'required');
    jQuery(".ebook-input [name='firstname']").attr('placeholder', 'First Name*');
    jQuery(".ebook-input [name='email']").attr('placeholder', 'Email*');

});