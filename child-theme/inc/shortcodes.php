<?php
/**
 * Smile Gallery [smile-gallery]
 */

if ( ! function_exists ( 'smile_gallery' ) ) {
  function smile_gallery(){
    ob_start();
    $swm_page_slug = swm_set_page_slug();
    $smile_gallery_slug = $swm_page_slug['smile_gallery'];
?>
<div class="smile-gallery-content">
  <?php
    $ctr =1;
        if( have_rows('smile_gallery','option') ):
        while( have_rows('smile_gallery','option') ): the_row();

        $before=get_sub_field('before_image','option');
        $after=get_sub_field('after_image','option');

    ?>

    <div class="smile-con">
        <?php if(get_sub_field('smile_title','option')) : ?>
        <span class="smile-title"><?php echo get_sub_field('smile_title','option'); ?></span>
        <?php endif; ?>
        <div>
            <a href="#test-popup<?php echo $ctr; ?>" class="group-gallery transition">
                <div class="gallery-single-outer">
                <i class="fa fa-search-plus transition"></i>

        <div class="1st gallery-single">
          <div class="gallery-image transition">
            <?php
              if ($before):
              echo swm_img_resize_src(202,195, $before);
              else:
              echo swm_placeholder(202,195);
              endif;
            ?>
          </div>
          <!-- <div class="gallery-head">Before</div> -->
        </div>
        <div class="gallery-single after">
          <div class="gallery-image transition">
            <?php
            if ($after):
            echo swm_img_resize_src(202,195, $after);
            else:
            echo swm_placeholder(202,195);
            endif;?>
          </div>
          <!-- <div class="gallery-head">After</div> -->
        </div>


                    <!--Portrait-->
          <?php /*if( have_rows('smile_add_image') ):
            $ctr_smile_image =1;
            $image_count_limit = 4;
            while( have_rows('smile_add_image','option') ): the_row();
            $smile_image = get_sub_field('smile_image','option');
            if($ctr_smile_image <= $image_count_limit):
          ?>
                <div class="2nd gallery-single <?php echo ($ctr_smile_image%2)? 'after':''; ?>">
                    <div class="gallery-image">
            <?php
              if ($smile_image) :
              echo swm_img_resize_src(370,300, $smile_image);
              else :
              echo swm_placeholder(370,300);
              endif;
            ?>
                    </div>
                </div>
         <?php endif; ?>

        <?php $ctr_smile_image++;
          endwhile;
          endif;*/
        ?>
                <!--Portrait-->


                  <div class="gallery-single">
                    <div class="gallery-head">Before</div>
                  </div>
                  <div class="gallery-single after">
                    <div class="gallery-head">After</div>
                  </div>


                </div>
            </a>
            <div class="gallery-single-outer smile-popup mfp-hide" id="test-popup<?php echo $ctr; ?>">
                <div class="3rd gallery-single">
                    <div class="gallery-image">
                        <?php
            if ($before) :
            echo swm_img_resize_src(250,180, $before, true, true, true);
            else :
            echo swm_placeholder(250,180);
            endif;
                        ?>
                    </div>
                    <!-- <div class="gallery-head">Before</div> -->
                </div>
                <div class="gallery-single after">
                    <div class="gallery-image">
                        <?php
              if ($after):
              echo swm_img_resize_src(250,180, $after, true, true, true);
              else :
              echo swm_placeholder(250,180);
              endif;
                        ?>
                    </div>
                    <!-- <div class="gallery-head">After</div> -->
                </div>

                <!--Portrait-->
                <?php
                  if( have_rows('smile_add_image') ):
                  $ctr_smile_image =1;
                  $image_count_limit = 4;

                  while( have_rows('smile_add_image','option') ): the_row();
                  $smile_image = get_sub_field('smile_image','option');

                   if($ctr_smile_image <= $image_count_limit):
                        ?>
                        <div class="4th gallery-single <?php echo ($ctr_smile_image==$image_count_limit)? 'after':''; ?>">
                            <div class="gallery-image">
                                <?php
                    if ($smile_image) :
                    echo swm_img_resize_src(250,180, $smile_image);
                    else :
                    echo swm_placeholder(250,180);
                    endif;
                                ?>
                            </div>
                        </div>
                <?php endif; ?>

                <?php $ctr_smile_image++;
                  endwhile;
                  endif;
                ?>
                <!--Portrait-->

                  <div class="gallery-single">
                    <div class="gallery-head">Before</div>
                  </div>
                  <div class="gallery-single after">
                    <div class="gallery-head">After</div>
                  </div>

                <?php if(get_sub_field('smile_content','option')): ?>
                <div class="smile-content-text"><?php echo get_sub_field('smile_content'); ?></div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php
            $ctr++;
            endwhile;
            endif; ?>


    <div class="cf"></div>
</div>

<?php
    $smilegallery = ob_get_clean();
    return $smilegallery;
  }
}


/**
 * Image [img]
 */
add_shortcode('img', 'img_shortcode');
if ( ! function_exists ( 'img_shortcode' ) ) {

  function img_shortcode( $atts ) {

      // Attributes
      extract( shortcode_atts(
          array(
              'imgid' => '',
              'w' => '350',
              'h' => '200',
              'align' => 'left',
              'crop' => 'c',
              'class' => '',
          ), $atts )
      );

      if ($align == 'left') :
          $a = 'alignleft';
      elseif ($align == 'right') :
          $a = 'alignright';
      elseif ($align == 'center') :
        $a = 'aligncenter';
      else :
          $a = 'alignleft';
      endif;
    // Upload image using url
    // $resultimg = media_sideload_image($img_url, $post_id, $desc);

     $cp = swm_crop_bfi($crop);
      ob_start();
  ?>

  <span class="video-wrapper <?php echo $a .' '. $class; ?>">
  <span class="video-inner">
  <?php if ($imgid) :
            $thumb = wp_get_attachment_image_src($imgid, $params);
           // $alt = get_post_meta($imgid) , '_wp_attachment_image_alt', true);
            //$imgtitle = get_post($imgid)->post_title;
            $imageurl = $thumb['0'];
            $img = '<img src="'.swm_get_img_url($imageurl).'" title="'.$imgtitle.'" alt="'.$alt.'">';

            echo $img;
        endif; ?>
</span>
</span>

  <?php
  $image = ob_get_clean();
      return $image;
  }

}

add_shortcode('inline-play-button','inline_play_button_shortcode');
function inline_play_button_shortcode($atts, $content = null){
  ob_start();
  // Attributes
  extract( shortcode_atts(
    array(
      'text' => 'See the Video',
      'transcript'=> '',
      'id' =>'',
    ), $atts )
         );
  $youtubeid = $id;
?>
<div class="play-btn-wrapper">
  <a href="//www.youtube.com/watch?v=<?php echo $youtubeid; ?>?rel=0&amp;autoplay=1&amp;controls=0&amp;showinfo=0&amp;wmode=transparent"  class="popup-video transition ">
    <i class="fa  fa-play-circle "></i> <?php echo $text; ?>
  </a>
  <div class="clearfix"></div>
  <?php if($content!=''|| $transcript!='') :?>
  <?php $rand = rand(5, 115); ?>
  <a href="#popup-<?php echo $rand; ?>" class="inline-popup-link" >Read Transcript</a>
  <?php endif; ?>
  <?php if($content!='' || $transcript!='') :?>
  <div class="popup-content mfp-hide" id="popup-<?php echo $rand; ?>">
    <p><?php echo ($content!='')? $content:''; ?> <?php echo ($transcript!='')? do_shortcode($transcript):''; ?></p>
  </div>
  <?php endif; ?>
</div>
<?php
  $clean = ob_get_clean();
  return $clean;
}


  function youtube_shortcode( $atts ) {

      // Attributes
      extract( shortcode_atts(
          array(
              'id' => '',
              'imgsrc' => 'yt-thumb',
              'w' => '350',
              'h' => '200',
              'align' => 'left',
              'crop' => 'c',
              'transcript' => '',
              'title' => '',
              'title_style' => 'top',
              'class' => '',
              'res' => 'hq',
          ), $atts )
      );

      if ($align == 'left') :
          $a = 'alignleft';
      elseif ($align == 'right') :
          $a = 'alignright';
      elseif ($align == 'center') :
        $a = 'aligncenter';
      else :
          $a = 'alignleft';
      endif;

        $img_url = 'http://img.youtube.com/vi/'.$id.'/'.$res.'default.jpg';

    // Upload image using url
    // $resultimg = media_sideload_image($img_url, $post_id, $desc);

    $folder = 'yt_thumb';
    $img = swm_create_thumb($img_url,$id,$folder,$w,$h,'',$crop);

    $cp = swm_crop_bfi($crop);
    $swm_options_testimonial_play_icon = get_field('swm_options_testimonial_play_icon','option');
    $swm_options_testimonial_transcript_button = get_field('swm_options_testimonial_transcript_button','option');
      ob_start();
  ?>

  <span class="video-wrapper <?php echo $a .' '. $class; ?>">
  <span class="video-inner">
  <?php if($title && $title_style == 'top') :?>
  <span class="video-title" style="max-width:<?php echo $w; ?>px"><?php echo $title; ?></span>
  <?php endif; ?>

<a href="//www.youtube.com/watch?v=<?php echo $id; ?>?rel=0&amp;autoplay=1&amp;controls=0&amp;showinfo=0&amp;wmode=transparent"  class="popup-video transition">
  <i class="fa <?php if($swm_options_testimonial_play_icon) : echo $swm_options_testimonial_play_icon; else : echo 'fa-play'; endif; ?> play-icon fa-play-absolute-center"></i>
  <?php if($title && $title_style == 'middle') :?>
  <span class="video-title-v2">
    <span class="tbl">
      <span class="tbl-cell">
        <?php echo $title; ?>
      </span>
    </span>
  </span>
  <?php endif; ?>


  <?php if ($imgsrc == 'yt-thumb') :

          echo $img;

        elseif ($imgsrc == 'f-thumb') :

          if ( has_post_thumbnail() ) :

            $params = array( $w, $h,'bfi_thumb' => true,'crop' => $cp, );
            $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID),$params);
            $alt = get_post_meta(get_post_thumbnail_id($post->ID) , '_wp_attachment_image_alt', true);
            $imgtitle = get_post(get_post_thumbnail_id($post->ID))->post_title;
            $imageurl = $thumb['0'];
            $img = '<img src="'.swm_get_img_url($imageurl).'" title="'.$imgtitle.'" alt="'.$alt.'">';

            echo $img;

          else :
            echo $img;
          endif;

        else :

            $upload_dir = wp_upload_dir();

            $imgsrc = $upload_dir['baseurl'] . $imgsrc;

            // Crop
            $params = array( 'width' => $w, 'height' => $h,'crop' => $cp, );
            $imageurl = bfi_thumb( $imgsrc, $params );
            $img = '<img src="'.swm_get_img_url($imageurl).'">';

            echo $img;

        endif; ?>


</a>


  <?php if($title && $title_style == 'bottom') :?>
  <span class="video-title" style="max-width:<?php echo $w; ?>px"><?php echo $title; ?></span>
  <?php endif; ?>

  <?php if($transcript) :?>
    <?php $rand = rand(5, 115); ?>
    <a href="#popup-<?php echo $rand; ?>" class="transcript-btn open-popup-link transition" style="max-width:<?php echo $w; ?>px;"><?php if($swm_options_testimonial_transcript_button ) : echo $swm_options_testimonial_transcript_button ; else : echo 'Read Transcript'; endif; ?></a>
  <?php endif; ?>

  </span>
  </span>


  <?php if($transcript) :?>
  <div class="popup-content mfp-hide" id="popup-<?php echo $rand; ?>">
    <div class="video-transcribe-container">
      <?php if($title){ ?>
        <span class="header-title"><?php  echo $title; ?></span>
      <?php } ?>
      <?php echo wpautop($transcript); ?>
    </div>
  </div>
  <?php endif; ?>

  <?php
  $video = ob_get_clean();
      return $video;
  }


/**
 * FAQ [faq]
 */
add_shortcode('faq', 'swm_faq_shortcode');

if ( ! function_exists ( 'swm_faq_shortcode' ) ) {
function swm_faq_shortcode(){

ob_start();
?>

<div class="faq-content">
  <?php wp_reset_query(); ?>
  <?php
  $args = array(
    'post_type' => 'faq',
    'post_per_page' => -1 ,
    'showposts' => -1,
    'orderby' => 'menu_order',
    'order' => 'ASC');
            // The Query
        $wp_query = new WP_Query( $args );

        ?>

  <ul class="list-question">

  <?php
  $ctr = 1;
  if ( $wp_query->have_posts()) : while ( $wp_query->have_posts()) :  $wp_query->the_post(); ?>
  <?php
  $gettitleTrim = trim(get_the_title());
  $get_title = strip_tags($gettitleTrim);
  ?>
  <li>
    <a href="#question<?php echo $ctr; ?>" class="smoothScroll"><strong><?php echo $get_title; ?></strong></a>
  </li>
  <?php $ctr++; endwhile; ?>

  <?php else : ?>

  <?php endif; ?>

  <?php wp_reset_query(); ?>

  </ul>

  <ul class="faq-list">

  <?php
  $ctr = 1;
  if ( $wp_query->have_posts()) : while ( $wp_query->have_posts()) :  $wp_query->the_post(); ?>
  <li class="faq-count">
    <a name="question<?php echo $ctr; ?>" class="anchor"></a>
  <div class="cf"></div>
  <span class="question-title">
      <strong>
        <span class="qw"> <?php the_title(); ?></span>
        <span class="cf"></span>
      </strong>
    </span>
    <div class="answer-con">

    <?php if( get_field('youtube_id') ) { 

      echo do_shortcode('[youtube id="'.get_field('youtube_id').'" align="right" crop="c" transcript="'.get_field('youtube_transcript').'" title="'.get_field('youtube_title').'" title_style="middle"]');

    } ?>

      <span class="ans"><strong>A:</strong></span> <?php the_content(); ?>
    </div>
    <div class="clearfix"></div>
  </li>
  <?php $ctr++; endwhile; ?>

  <?php else : ?>

  <?php endif; ?>

  <?php wp_reset_query(); ?>

  </ul>

</div>

<?php
  $faq = ob_get_clean();
  return $faq;
}
}


add_shortcode('new_patient_assistant', 'newpatientassistant_shortcode');
function newpatientassistant_shortcode( $atts ) {
ob_start();

?>
<?php if(get_field("np_show")){ ?>
  <?php if(get_field("np_link")){ ?>
  <a href="<?php echo get_field('np_link'); ?>">
  <?php } ?>
  <div class="offer-wrapper"><span><?php echo get_field("np_title"); ?></span>
    <div class="cf"></div>
    <?php echo wpautop( get_field( "np_content" ) ); ?>
    <div class="cf"></div>
  </div>
  <?php if(get_field("np_link")){ ?>
  </a>
  <?php } ?>
<?php } ?>

  <?php
  $video = ob_get_clean();
      return $video;
  }


add_shortcode( 'iframe_embed', 'iframe_embed_shortcode' );
function iframe_embed_shortcode($atts){
  ob_start();
     // Attributes
    extract( shortcode_atts(
      array(
        'src' => '',
        'width' => 630,
        'height'=>354,
     
      ), $atts )
           );
  if($src!=''){
  ?>
  <div class="iframe-e-wrapper">
 <iframe class='sproutvideo-player' src='<?php echo $src; ?>' width='<?php echo $width; ?>' height='<?php echo $height; ?>' frameborder='0' allowfullscreen></iframe>
  </div>
  <?php
  }
  
  
$clean = ob_get_clean();
  return $clean;
}
  

add_shortcode('dental_saving','dental_saving_func');
function dental_saving_func($atts){
ob_start(); 

if( have_rows('dental_saving') ): ?>
  <div class="dental-plan-container">
    <?php while ( have_rows('dental_saving') ) : the_row();
      $promo_title = get_sub_field('promo_title');
      $amount_year = get_sub_field('amount_year');
      $annual_savings_year = get_sub_field('annual_savings_year');
      $amount_month = get_sub_field('amount_month');
      $annual_savings_month = get_sub_field('annual_savings_month');
      $specials_includes = get_sub_field('specials_includes'); 

      if( ($amount_year) && (!$amount_month) ) {
        $link = "one-link";
      }else if ( (!$amount_year) && ($amount_month) ) {
        $link = "one-link";
      }else if ( ($amount_year) && ($amount_month) ) {
        $link = "two-link";
      } else {
        $link = "none";
      }
      ?>
        <div class="dental-plan">
          <div class="plan-title"><?php echo $promo_title ?></div>
          <?php if($link!="none"){ ?>
          <div class="plan-savings">
            <ul class="<?php echo $link; ?>">
              <?php if($amount_year){ ?>
              <li>
                <div class="dental-amount"><?php echo $amount_year; ?> / Year</div>
                <div class="dental-desc-saving">Total Annual Savings: <?php echo $annual_savings_year; ?>**</div>
              </li>
              <?php } ?>
              <?php if($amount_month){ ?>
              <li>
                <div class="dental-amount"><?php echo $amount_month; ?> / Month</div>
                <div class="dental-desc-saving">Total Annual Savings: <?php echo $annual_savings_month; ?>**</div>
              </li>
              <?php } ?>
            </ul>
          </div>
        <?php } ?>
          <div class="dental-plan-content">
              <?php echo $specials_includes; ?>
          </div>
        </div>
    <?php endwhile; ?>
  </div>
<?php endif;

 $clean = ob_get_clean();
  return $clean;
}


/**
 * Offer/Specials [offer-2]
 */
add_shortcode( 'offer-2', 'offer_2_shortcode' );

if ( ! function_exists ( 'offer_2_shortcode' ) ) {

  function offer_2_shortcode( $atts ) {

      // Attributes
      extract( shortcode_atts(
        array(
            'title' => '',
            'precontent' => '',
            'content' => '',
            'amount' => '',
            'valueof' => '',
            'date' => '',
            'size' => 'short',
            'style' => '1',
            'phone' => '',
            'disclaimer' => '',
            'popcontent' => '',
            'class' => '',
        ), $atts )
      );

      if ($style == '1') :
        $designstyle = 'offer-event-a';
      elseif ($style == '2') :
        $designstyle = 'offer-event-b';
      elseif ($style == '3') :
        $designstyle = 'offer-event-c';
      else :
        $designstyle = 'offer-event-d';
      endif;

      if ($size == 'short') :
          $designsize = 'swm-col-2';
      elseif ($size == 'large') :
        $designsize = 'full-width';
      else :
          $designsize = 'swm-col-2';
      endif;

      if ($date) :
          $date = $date;
      else :
          $date = '';
      endif;


      $rand = rand(0,100);


      if ($disclaimer) :
          $disclaimer = $disclaimer;
      else :
          $disclaimer = '<a class="popup-with-zoom-anim" href="#small-dialog-'.$rand.'"> View Details</a>';
      endif;

      ob_start();
  ?>

  <?php if($title && $style == '1') { ?>
  <div class="offer-event <?php echo $designsize; ?> offer-module">
    <div class="list-item-container">
      <div class="list-item <?php echo $designstyle; ?>">
        <div class="content">
          <span class="fa offer-gift horizontal-center">Special Offer</span>
          <div class="content-spacer">
            <div class="event-info-campaign">
              <h4><?php echo $title; ?></h4>
              <div class="event-price">Value <?php if($amount) : echo "$"; echo $amount; endif; ?></div>
              <div class="cf"></div>
            </div>
            <div class="event-info-campaign-footer">
              <div class="appt-button"><?php echo swm_schedule_btn('btn-default-dark large calendar desktop-mode'); ?></div>
              <div class="event-desc"><p><?php echo $precontent; ?> <?php if ($phone == 'no') : ; else : echo do_shortcode('[phone]'); endif; ?> <?php echo $content; ?></p></div>
              <?php echo swm_schedule_btn('btn-default-dark large calendar mobile-mode'); ?>
              <div class="event-promo-ends"><?php if($date){ ?>Promotion ends <?php echo $date; ?> <?php } ?><div class="event-disclaimer"> <?php echo $disclaimer; ?></div></div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php } else if($title && $style == '2') { ?>
  <div class="offer-event <?php echo $designsize; ?> offer-module">
    <div class="list-item-container">
      <div class="list-item <?php echo $designstyle; ?>">
        <div class="content">
          <span class="fa offer-gift horizontal-center">Special Offer</span>
          <div class="event-price"><div class="event-position-handler"><?php if($amount) : echo "$"; echo $amount; endif; ?> <span class="info">Value</span></div></div>
          <div class="content-spacer">
            <div class="event-info-campaign">
              <h4><?php echo $title; ?></h4>
              <div class="cf"></div>
            </div>
            <div class="event-info-campaign-footer">
              <div class="appt-button"><?php echo swm_schedule_btn('btn-default-dark large calendar desktop-mode'); ?></div>
              <div class="event-desc"><p><?php echo $precontent; ?> <?php if ($phone == 'no') : ; else : echo do_shortcode('[phone]'); endif; ?> <?php echo $content; ?></p></div>
              <?php echo swm_schedule_btn('btn-default-dark large calendar mobile-mode'); ?>
              <div class="event-promo-ends"><?php if($date){ ?>Promotion ends <?php echo $date; ?> <?php } ?><div class="event-disclaimer"> <?php echo $disclaimer; ?></div></div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php } else if($title && $style == '3') { ?>
  <div class="offer-event <?php echo $designsize; ?> offer-module">
    <div class="list-item-container">
      <div class="list-item <?php echo $designstyle; ?>">
        <div class="content">
          <span class="fa offer-gift horizontal-center">Special Offer</span>
          <div class="content-spacer">
            <div class="event-info-campaign">
              <h4><?php echo $title; ?></h4>
              <div class="event-price">Value <?php if($amount) : echo "$"; echo $amount; endif; ?></div>
              <div class="cf"></div>
            </div>
            <div class="event-info-campaign-footer">
              <div class="event-desc"><p><?php echo $precontent; ?> <?php if ($phone == 'no') : ; else : echo do_shortcode('[phone]'); endif; ?> <?php echo $content; ?></p></div>
              <?php echo swm_schedule_btn('btn-default-dark large calendar'); ?>
              <div class="event-promo-ends"><?php if($date){ ?>Promotion ends <?php echo $date; ?> <?php } ?><div class="event-disclaimer"> <?php echo $disclaimer; ?></div></div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php } else if($style == '5') { ?>
  <div class="offer-event <?php echo $designsize; ?> offer-module sidepop">
    <div class="list-item-container">
      <div class="list-item <?php echo $designstyle; ?>">
        <div class="content">
          <div class="content-spacer">
            <div class="event-price">
              <div class="price-handler"><?php if($amount) : echo "$"; echo $amount; endif; ?></div>
            <div class="event-info-campaign">
              <h4><?php echo $title; ?></h4>
              <div class="cf"></div>
              <span class="info"><?php echo $valueof; ?></span></div>
            </div>
            <div class="event-info-campaign-footer">
              <div class="event-promo-ends"><?php if($date){ ?>Promotion ends <br /><?php echo $date; ?> <?php } ?>
                <div class="event-disclaimer"> <?php echo $disclaimer; ?></div>
              </div>
              <div class="cf"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php } else /* Load Module 4 */ { ?>
  <div class="offer-event <?php echo $designsize; ?> offer-module">
    <div class="list-item-container">
      <div class="list-item <?php echo $designstyle; ?>">
        <div class="content">
          <span class="fa offer-gift horizontal-center">Special Offer</span>
          <div class="content-spacer">
            <div class="event-price">
              <div class="price-handler"><?php if($amount) : echo "$"; echo $amount; endif; ?></div>
              <span class="info"><?php echo $valueof; ?></span></div>
            <div class="event-info-campaign">
              <h4><?php echo $title; ?></h4>
              <div class="cf"></div>
            </div>
            <div class="event-info-campaign-footer">
              <div class="event-desc"><p><?php echo $precontent; ?> <?php if ($phone == 'no') : ; else : echo do_shortcode('[phone]'); endif; ?> <?php echo $content; ?></p></div>
              <?php echo swm_schedule_btn('btn-default-dark large calendar'); ?>
              <div class="event-promo-ends"><?php if($date){ ?>Promotion ends <?php echo $date; ?> <?php } ?> <div class="event-disclaimer"> <?php echo $disclaimer; ?></div></div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php } ?>

  

  <div id="small-dialog-<?php echo $rand; ?>" class="small-dialog zoom-anim-dialog mfp-hide popup-offer">
    <div class="event-price"><?php if($amount) : echo "$"; echo $amount; endif; ?></div>
    <h4><?php echo $title; ?></h4>
    <span class="info"><?php echo $valueof; ?></span>
    <p class="pop-desc"><?php echo $popcontent; ?></p>
    <p class="event-desc"><?php echo $precontent; ?> <?php if ($phone == 'no') : ; else : echo do_shortcode('[phone]'); endif; ?> <?php echo $content; ?></p>
    <?php echo swm_schedule_btn('btn-default-dark large calendar'); ?>
    <div class="event-promo-ends"><?php if($date){ ?>Promotion ends <?php echo $date; ?> <?php } ?></div>
    <?php if($style == '5') : ?><a class="popup-with-zoom-anim custom-back" href="#small-dialog-main-button"><i class="fa fa-angle-double-left"></i> Back</a><?php endif; ?>
  </div>

  <?php
  $offer_2 = ob_get_clean();
      return $offer_2;
  }

}