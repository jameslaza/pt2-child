<?php 

require get_stylesheet_directory(). '/inc/shortcodes.php';
require get_stylesheet_directory(). '/inc/external_link.php';

/**
* Register fields
*/
function swm_add_fields() {
	swm_register_fields('fld-layout-builder2');
}


/**
* DeRegister fields
*/
function swm_deregister_fields() {
	$swm_deregister_fields = array(
		 'fld-layout-builder',
	);
	return $swm_deregister_fields;
}


/**
* Set child css and javascript
*/
function swm_child_style() {
    $var = null;

    wp_dequeue_script('sticky-kit-script');
    wp_deregister_script('sticky-kit-script');

	//wp_enqueue_style( 'child-layout', get_stylesheet_directory_uri(). '/layout.css', array(), $var );
	wp_enqueue_script( 'child-script', get_stylesheet_directory_uri() .'/js/child-script.js', array(), $var, true );
	wp_enqueue_script( 'sticky-kit-script2', get_stylesheet_directory_uri() .'/js/sticky-kit.js', array(), $var, true );
}
add_action( 'wp_enqueue_scripts', 'swm_child_style',23);


/**
 * Enqueue scripts and styles.
 */
if ( ! function_exists ( 'swm_layout_header_scripts' ) ) {
	function swm_layout_header_scripts() {
		$var = null;
		$header_layout = get_field('swm_options_header_layout','options');
		$header_layout = ($header_layout!='')? $header_layout : '';
		if($header_layout!=""){ 
			wp_enqueue_style( 'header-layout', get_template_directory_uri(). '/layouts/header/layout-'.$header_layout.'.css', array(), $var );
			//wp_enqueue_style( 'header-child-layout', get_stylesheet_directory_uri(). '/layouts/header/layout-'.$header_layout.'.css', array('header-layout'), $var );
		}
	}
	add_action( 'wp_enqueue_scripts', 'swm_layout_header_scripts',14 );
}

/**
* Load Template Layouts
*/
function acf_load_default_layout($value) {
	if ($value !== NULL) {
		return $value;
	}
	
	/** List of modules
	* swm_main_content_module
	* swm_generic_content_module
	* swm_trio_blocks_module
	* swm_generic_intro_plus_video_module
	* swm_featured_links_module
	* swm_all_services_module
	* swm_rate_us_module
	* swm_info_bar_module
	* swm_tour_office_module
	* swm_doctors_module
	* swm_accreditation_module
	* swm_birdeye_module
	* swm_faq_module
	* swm_smile_gallery_module
	*/
	
	$type = get_post_type();
	$load_template = get_page_template_slug($post_id);
	// If pages are auto generated these modules are loaded
	if ($load_template=="templates/front-page-template.php") {
		// Front Page
		$value = [0 => 
			// Show order of modules, rearrange, add or remove from the list
			['acf_fc_layout' => 'swm_doctors_module',],
			['acf_fc_layout' => 'swm_generic_content_module',],
			['acf_fc_layout' => 'swm_featured_links_module',],
			['acf_fc_layout' => 'swm_trio_blocks_module',],
			['acf_fc_layout' => 'swm_trio_blocks_module',],
			['acf_fc_layout' => 'swm_all_services_module',],
			['acf_fc_layout' => 'swm_rate_us_module',],
			['acf_fc_layout' => 'swm_info_bar_module',]
		];
		
    } elseif ($load_template=="templates/contact-us-template.php") {
		// Contact Page
		$value = [1 => 
			// Show order of modules, rearrange, add or remove from the list

			['acf_fc_layout' => 'swm_main_content_module',],
			['acf_fc_layout' => 'swm_rate_us_module',],
			['acf_fc_layout' => 'swm_info_bar_module']
		];
		
    } elseif ($type=="page") {
		// Default Page
		$value = [2 => 
			// Show order of modules, rearrange, add or remove from the list
			['acf_fc_layout' => 'swm_main_content_module',],
			['acf_fc_layout' => 'swm_ebook_module',],
			['acf_fc_layout' => 'swm_birdeye_module',],
			['acf_fc_layout' => 'swm_rate_us_module',],
			['acf_fc_layout' => 'swm_info_bar_module']
		];
		
    }
	return $value;
}
add_filter('acf/load_value/name=swm_build_section', 'acf_load_default_layout', 20, 3);
add_filter( 'force_filtered_html_on_import' , false );


function remove_empty_links( $menu ) {
    return str_replace( '<a href="#">', '<a>', $menu );
}

add_filter( 'wp_nav_menu_items', 'remove_empty_links' );



/**
* Setup CPT
*/
if ( ! function_exists ( 'swm_set_cpt' ) ) {
	function swm_set_cpt(){
	$swm_cpt = array(
		'staffs' => array('slug' => 'about-us/meet-the-team','singular_name' => 'Meet the Team'),
		'testimonial' => array('slug' => 'patient-testimonial','singular_name' => 'Testimonial'),
	    );

	return $swm_cpt;
	}
}

function swm_set_page_slug(){
    $swm_page_slug = array(
        'testimonial' => 'patient-testimonials',
        );
    return $swm_page_slug;
  }


/**
* Remove CPT
*/
function custom_unregister_theme_post_types() {
global $wp_post_types;
	if ( isset( $wp_post_types["news"] ) ) {
	unset( $wp_post_types[ "news" ] ); //UPDATED
	}
}
add_action( 'init', 'custom_unregister_theme_post_types', 20 );



/**
 * Video Link [inline-featured-video]
*/
add_shortcode( 'inline-featured-video', 'inlinefeaturedvideo_shortcode' );
function inlinefeaturedvideo_shortcode( $atts ) {
  // Attributes
  extract( shortcode_atts(
    array(
      'id' => '',
      'text' => '',
    'transcript' => '',
    ), $atts )
  );
  ob_start();
?>
<div class="inline-featured-video-holder">
<a href="//www.youtube.com/watch?v=<?php echo $id; ?>?rel=0&amp;autoplay=1&amp;controls=0&amp;showinfo=0&amp;wmode=transparent"  class="popup-video play-video"><?php echo $text; ?></a>
<div class="cf"></div>
<?php if($transcript) :?>
  <?php $rand = rand(5, 115); ?>
  <a href="#popup-<?php echo $rand; ?>" class="featured-video-transcript-btn open-popup-link transition">Read Transcript</a>
<?php endif; ?>

<?php if($transcript) :?>
  <div class="popup-content mfp-hide" id="popup-<?php echo $rand; ?>">
    <div class="popup-content-overflow"><?php echo wpautop($transcript); ?></div>
  </div>
<?php endif; ?>
</div>
<?php
$videolink = ob_get_clean();
  return $videolink;
}
