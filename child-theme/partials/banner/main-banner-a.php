<!-- Main Banner A -->
<?php 
// Desktop Size
$d_width = 1584;
$d_height = 600;
// Mobile Size - Note if exact size is declared image will load as original, no cropping will be done.
$m_width = 991;
$m_height = 590;
$swm_main_banner_background = get_field('swm_main_banner_background');
$swm_main_banner_background_mobile = get_field('swm_main_banner_background_mobile');
$swm_main_banner_height_size = get_field('swm_main_banner_height_size');
$swm_main_banner_background_filter = get_field('swm_main_banner_background_filter');
$swm_main_banner_background_color = get_field('swm_main_banner_background_color');
$swm_main_banner_title = get_field('swm_main_banner_title');
$swm_main_banner_content = get_field('swm_main_banner_content');
$swm_main_banner_left_label = get_field('swm_main_banner_left_label');
$swm_main_banner_left_link = get_field('swm_main_banner_left_link');
$swm_main_banner_right_label = get_field('swm_main_banner_right_label');
$swm_main_banner_right_link = get_field('swm_main_banner_right_link');
$swm_main_banner_button_url = get_field('swm_main_banner_button_url');
$swm_main_banner_additional_info = get_field('swm_main_banner_additional_info');
$swm_main_banner_cta_box_title = get_field('swm_main_banner_cta_box_title');
$swm_main_banner_box_button_label = get_field('swm_main_banner_box_button_label');

$get_background_image = swm_img_resize_url($d_width,$d_height,$swm_main_banner_background) ;
$get_background_image_mobile = swm_img_resize_url($m_width,$m_height,$swm_main_banner_background_mobile) ;
?>
<?php if ($get_background_image_mobile) : ?>
	<style>
		@media only screen and (max-width: 991px) {
			.mobile-banner {
				background-image:url('<?php echo $get_background_image_mobile; ?>') !important;
				background-position: center;
			}
		}
	</style>
<?php endif; ?>

<section class="banner-1 banner-type-container dark-palette position-relative main-banner current-theme banner-fade-background mobile-banner" <?php echo swm_section_module_bg($swm_main_banner_background_color,$get_background_image,$swm_main_banner_height_size); ?>>
	<div class="load-filter container absolute-center">
		<div class="row">
			<div class="col-md-6 banner-1-left banner-1-matchHeight">
				<div class="banner-text-content">
					<?php
						echo ($swm_main_banner_title) ? '<span class="header-title">' . $swm_main_banner_title . '</span>' : '' ;
					?>
					<?php if ($swm_main_banner_content) : ?><p><?php echo do_shortcode( $swm_main_banner_content ); ?></p><?php endif; ?>
					<?php if($swm_main_banner_left_label) : ?>

					<?php if($swm_main_banner_left_link || $swm_main_banner_right_link){ ?>
					<ul class="hero-link">
						<?php if($swm_main_banner_left_link){ ?>
						<li><a href="<?php echo $swm_main_banner_left_link; ?>"><?php echo $swm_main_banner_left_label; ?></a></li>
						<?php } ?>
						<?php if($swm_main_banner_right_link){ ?>
						<li><a href="<?php echo $swm_main_banner_right_link; ?>"><?php echo $swm_main_banner_right_label; ?></a></li>
						<?php } ?>
					</ul>
					<?php } ?>

					<?php endif; ?>
				</div>
			</div>
			<div class="col-md-6 banner-1-right banner-1-matchHeight">
				<div class="schedule-online">
					<div class="content">
						<h3><?php if($swm_main_banner_cta_box_title !='') : echo $swm_main_banner_cta_box_title; else : echo swm_info_bar_cta_title(); endif; ?></h3>
						<?php echo swm_mobile_btn('btn-text mobile large'); ?>
						<?php echo swm_schedule_btn('btn-default-dark large calendar','','', $swm_main_banner_box_button_label); ?>
						<?php echo swm_info_bar_additional_info('<p>','</p>'); ?>
					</div>
					<div class="footer">
						<div class="schedule">
						<?php echo swm_business_hours('short'); ?>
						</div>
						<div class="clearfix"></div>
					</div>
					<?php echo do_shortcode('[new_patient_assistant]'); ?>
				</div>
			</div>
		</div>
	</div>
</section>