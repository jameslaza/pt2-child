<!-- Trio Blocks A -->
<section class="niche-blocks featured-services-type2 featured-services-container light-palette">
	<div class="section-box">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php 
						$limit = 250;
						$swm_trio_blocks_title_intro = get_sub_field('swm_trio_blocks_title_intro');
						$swm_trio_blocks_content_intro = get_sub_field('swm_trio_blocks_content_intro');
					?>
					<!-- Intro Title and Content -->
					<?php if($swm_trio_blocks_title_intro || $swm_trio_blocks_content_intro) : ?>
					<div class="container">
						<div class="mod-content-fullwidth">
							<?php if($swm_trio_blocks_title_intro) : echo '<span class="section-title">'.$swm_trio_blocks_title_intro.'</span>'; endif; ?>
							<?php echo $swm_trio_blocks_content_intro; ?>
						</div>
					</div>
					<?php endif; ?>

					<?php if( have_rows('swm_trio_blocks_column') ): ?>

					<div class="fs-list-type2">
						<div class="fs-box-container box-container swm-col-3">

					<?php while ( have_rows('swm_trio_blocks_column') ) : the_row(); ?>

						<?php 
						$swm_trio_blocks_image = get_sub_field('swm_trio_blocks_image');
						$swm_trio_blocks_target_page = get_sub_field('swm_trio_blocks_target_page');
						$swm_trio_blocks_title = get_sub_field('swm_trio_blocks_title');
						$swm_trio_blocks_content = (get_sub_field('swm_trio_blocks_content')) ? get_sub_field('swm_trio_blocks_content') : '<p>' . swm_custom_excerpt($tagget_page_content,$limit) . '</p>' ;
						$swm_trio_blocks_button_label = get_sub_field('swm_trio_blocks_button_label');
						$swm_trio_blocks_button_link = get_sub_field('swm_trio_blocks_button_link');

						$get_image = swm_img_resize_obj(350,177,$swm_trio_blocks_image);
						?>

						<div class="list-item">
							<?php if($swm_trio_blocks_target_page !='') { ?>
							<a href="<?php echo $swm_trio_blocks_target_page; ?>" class="transition">
								<div class="list-item-img">
									<?php echo $get_image; ?>
								</div>
							</a>
							<?php } else { ?>
								<div class="list-item-img">
									<?php echo $get_image; ?>
								</div>
							<?php } ?>
							<div class="clear"></div>
							<div class="content">
								<div class="inner-content" data-mh="trio-blocks-a">
									<div class="niche-title-container"><span class="title"><?php echo $swm_trio_blocks_title; ?></span></div>
									<div class="excerpt-container"><?php echo $swm_trio_blocks_content; ?></div>
								</div>
								<a href="<?php echo $swm_trio_blocks_button_link; ?>" class="btn-flat-dark"><?php if($swm_trio_blocks_button_label) : echo $swm_trio_blocks_button_label; else : echo 'Learn More'; endif; ?></a>
							</div>
						</div>
					
					<?php endwhile; ?>

						</div>
					</div>

					<?php endif; ?>
						
				</div>
			</div>
		</div>
	</div>
</section>