<!-- Smile Gallery Slider -->
<?php 
	$smile_gallery = get_field('smile_gallery','option');
	$swm_options_smile_gallery_slider_background_color = get_field('swm_options_smile_gallery_slider_background_color','option');
	$swm_options_smile_gallery_slider_section_title = get_field('swm_options_smile_gallery_slider_section_title','option');
?>
	<section class="smile-gallery-slider light-palette current-theme" <?php echo swm_section_module_bg($swm_options_smile_gallery_slider_background_color); ?>>
		<div class="section-box">
			<div class="container">
				<div class="row">
					<div class="mod-content-fullwidth">
					<?php if($swm_options_smile_gallery_slider_section_title) : echo '<h2>'.$swm_options_smile_gallery_slider_section_title.'</h2>'; endif; ?>
					</div>
					<div class="col-md-12">
						<div class="owl-carousel smile-gallery-slider-carousel owl-theme">

							<?php $ctr =1; if( have_rows('smile_gallery','option') ): ?>
							<?php while( have_rows('smile_gallery','option') ): the_row();
								$before=get_sub_field('before_image','option');
								$after=get_sub_field('after_image','option');
						    ?>
								<div>
								<div class="item">
									<a href="#popup<?php echo $ctr; ?>" class="group-gallery-carousel transition">
									  <span class="gallery-single-outer">
										<span>
											<span class="smile-overlay">
												<?php
												if ($before):
												  echo swm_img_resize_src(300,200, $before);
												else:
												  echo swm_placeholder(300,200);
												endif ?>
											</span>
											<div class="gallery-head">Before</div>
										</span>
										<span>
											<span class="smile-overlay">
												<?php
												if ($after):
												  echo swm_img_resize_src(300,200, $after);
												else:
												  echo swm_placeholder(300,200);
												endif;?>
											</span>
											<div class="gallery-head after">After*</div>
										</span>
									  </span>
									</a>
								</div>
								</div>
								
								
							
							<?php $ctr++;  endwhile; ?>
							<?php endif; ?>
						</div><!-- end owl-carousel -->
						
						<?php $ctr =1; if( have_rows('smile_gallery','option') ): ?>
						<?php while( have_rows('smile_gallery','option') ): the_row();
							$before=get_sub_field('before_image','option');
							$after=get_sub_field('after_image','option');
						?>
						<div class="mfp-hide">

							<div class="gallery-single-outer smile-popup" id="popup<?php echo $ctr; ?>">
								<div class="gallery-single">
									<span>
										<span class="smile-overlay">
											<?php
											  if ($before) :
												echo swm_img_resize_src(300,200, $before);
											  else :
												echo swm_placeholder(300,200);
											  endif;
											  ?>
										  </span>
										<!-- <span class="smile-labels">Before</span> -->
									</span>
									<span>
										<span class="smile-overlay">
											<?php
										  if ($after):
											echo swm_img_resize_src(300,200, $after);
										  else :
											echo swm_placeholder(300,200);
										  endif;
										  ?>
										</span>
										<!-- <span class="smile-labels smile-after">After</span> -->
									</span>


                <!--Portrait-->
                <?php 
                  if( have_rows('smile_add_image') ):
                  $ctr_smile_image =1;
                  $image_count_limit = 4;

                  while( have_rows('smile_add_image','option') ): the_row();
                  $smile_image = get_sub_field('smile_image','option');
                  
                   if($ctr_smile_image <= $image_count_limit):
                        ?>
                    <span>
						<span class="smile-overlay">
	                        <?php
				                if ($smile_image) :
				                echo swm_img_resize_src(300,200,'c', $smile_image);
				                else :
				                echo swm_placeholder(300,200);
				                endif;
	                        ?>
                        </span>
					</span>
                <?php endif; ?>

                <?php $ctr_smile_image++;
                  endwhile;
                  endif;
                ?>
                <!--Portrait-->


				<div class="smile-overlay-text">Before</div>
				<div class="smile-overlay-text after">After*</div>

									
									<div class="cf"></div>
									<?php if(get_sub_field('smile_content','option')): ?>
									<div class="smile-description">
										<?php echo get_sub_field('smile_content'); ?>
									</div>
									<?php endif; ?>

								</div>
							</div>

						</div>
						<?php $ctr++;  endwhile; ?>
						<?php endif; ?>
						

						<div class="cf"></div>
						<?php if( get_field('swm_options_smile_gallery_slider_link','option') ) { ?>
						<span class="display-block text-center">
							<a href="<?php echo get_field('swm_options_smile_gallery_slider_link','option'); ?>" class="btn-text-dark"><?php echo get_field('swm_options_smile_gallery_slider_link_label','option'); ?> <i class='fa fa-chevron-circle-right' aria-hidden='true'></i></a>
						</span>
					<?php } ?>
					</div>
					<!-- arrow down here -->
				</div>
			</div>
		</div>
	</section>
<?php ?>