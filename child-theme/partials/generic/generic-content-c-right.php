<!-- Generic Content C (Right) -->
<?php 
// Featured Image Width
$d_width = 570;
$d_height = 600;
$swm_generic_content_featured_image = get_sub_field('swm_generic_content_featured_image');
$swm_generic_content_background_image = get_sub_field('swm_generic_content_background_image');
$swm_generic_content_background_color = get_sub_field('swm_generic_content_background_color');
$swm_generic_content_title = get_sub_field('swm_generic_content_title');
$swm_generic_content_banner_content = get_sub_field('swm_generic_content_banner_content');
$swm_generic_content_left_link_label = get_sub_field('swm_generic_content_left_link_label');
$swm_generic_content_left_link = get_sub_field('swm_generic_content_left_link');
$swm_generic_content_right_link_label = get_sub_field('swm_generic_content_right_link_label');
$swm_generic_content_right_link = get_sub_field('swm_generic_content_right_link');
$swm_generic_content_class = get_sub_field('swm_generic_content_class');
$get_featured_image = swm_img_resize_obj($d_width,$d_height,$swm_generic_content_featured_image) ;
?>
<section class="generic-content-c dark-palette generic-content-c-right-image current-theme <?php echo $swm_generic_content_class; ?>" <?php echo swm_section_module_bg($swm_generic_content_background_color); ?>>
	<div class="section-box load-filter">
		<div class="generic-c-container">
			<div class="row">
				<div class="col-md-8 left-col generic-content-c-matchHeight position-relative">
					<div class="generic-content absolute-center">
						<?php
							echo ($swm_generic_content_title) ? '<h3>' . $swm_generic_content_title . '</h3>' : '' ;
							echo $swm_generic_content_banner_content;
						?>
						<?php /*echo ($swm_generic_content_left_link_label) ? '<a href="'. $swm_generic_content_left_link .'" class="btn-default-light">' . $swm_generic_content_left_link_label . '</a>' : '' ; ?>
						<?php echo ($swm_generic_content_right_link_label) ? '<a href="'. $swm_generic_content_right_link .'" class="generic-link">' . $swm_generic_content_right_link_label . '</a>' : '' ; */?>
					</div>
				</div>
				<div class="col-md-4 right-col generic-content-c-matchHeight">
					<div class="generic-content-c-img-container">
						<?php echo $get_featured_image; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>