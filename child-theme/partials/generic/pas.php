		<?php 
			$get_ebook = get_sub_field('ebook');
			$ebook = $instance['ebook'];
			$ebook = $get_ebook->post_name;

			if(!$ebook){
				$ebook = get_option('widget_swm_ebook_sidebar');
				$ebook = $ebook[2]['ebook'];
			}


			  $args = array(
				'name'        => $ebook,
				'post_type'   => 'ebook',
				'post_status' => 'publish',
				'numberposts' => 1
			  );

			  $the_query = new WP_Query( $args );
			  if ( $the_query->have_posts() ) :
?>

<!-- Ebook Section - Fullwidth  -->
<section class="ebook-fullwidth-container dark-palette current-theme">
  <div class="container">
    <div class="row">
			<?php

			  if ( $the_query->have_posts() ) :
			  while ( $the_query->have_posts() ) : $the_query->the_post();

				$title = get_field('ebook_main_title');
				$content = get_the_content();

				// Form Platform
				$ebook_form_platform = get_field('ebook_form_platform');

				// Ebook Form Infusion Form
				$form_action = get_field('ebook_form_action');
				$form_id = get_field('ebook_form_id');
				$form_name = get_field('ebook_form_name');
				$infusionsoft_version = get_field('ebook_infusionsoft_version');
				$types = get_field('ebook_types');
				$custom_fields = get_field('ebook_custom_fields');
				$additional_fields = get_field('ebook_additional_fields');

				$ebook_submit_button = get_field('ebook_submit_button');

				// Ebook AC Form
				$type_ac = get_field('ebook_form_type_ac');
				$form_action_ac = get_field('ebook_form_action_ac');
				$form_id_ac = get_field('ebook_form_id_ac');
				$fnid_ac = get_field('ebook_form_fnid_ac');
				$eid_ac = get_field('ebook_form_eid_ac');
				$cid_ac = get_field('ebook_form_cid_ac');
				$custom_form_ac = get_field('ebook_custom_form_ac');
				$stype_ac = get_field('ebook_form_stype_ac');
				$custom_script_ac = get_field('ebook_custom_script_ac');

				// Custom Form
				$ebook_custom_form = get_field('ebook_custom_form');

				if ( has_post_thumbnail() ) :
				  $alt = get_post_meta(get_post_thumbnail_id($post->ID) , '_wp_attachment_image_alt', true);
				  $imgtitle = get_post(get_post_thumbnail_id($post->ID))->post_title;
				  $imageurl = get_post(get_post_thumbnail_id($post->ID))->guid;
				endif;

				// Array Infusion Form
				$ebook_form = array(
				  'form_action' => $form_action,
				  'form_id' => $form_id,
				  'form_name' => $form_name,
				  'infusionsoft_version' => $infusionsoft_version,
				  'types' => $types,
				  'custom_fields' => $custom_fields,
				  'additional_fields' => $additional_fields,
				  'ebook_submit_button' => $ebook_submit_button
				);
				
				// Array AC Form
				$ebook_form_ac = array(
				  'type_ac' => $type_ac,
				  'form_action_ac' => $form_action_ac,
				  'form_id_ac' => $form_id_ac,
				  'fnid_ac' => $fnid_ac,
				  'eid_ac' => $eid_ac,
				  'cid_ac' => $cid_ac,
				  'custom_form_ac' => $custom_form_ac,
				  'stype_ac' => $stype_ac,
				  'custom_script_ac' => $custom_script_ac,
				  'ebook_submit_button' => $ebook_submit_button
				);

				if ($ebook_form_platform == 'Infusionsoft Form') {
				  $form = get_ebook_form($ebook_form);
				}elseif ($ebook_form_platform == 'Active Campaign Form') {
				  $form = get_ebook_form_ac($ebook_form_ac);
				}elseif ($ebook_form_platform == 'Custom Form') {
				  $form = $ebook_custom_form;
				}else {
				  $form = get_ebook_form($ebook_form);
				}

				$attr = array(
					  'image' => array (
							'alt' => $alt,
							'title' => $imgtitle,
							'src' => $imageurl
							),
					  'title' => $title,
					  'content' => $content,
					  'form' => $form
					  );

				$output = get_ebook($attr);

			  endwhile;
			  endif;
			  wp_reset_query();

			  echo $output;
		
		?>
    </div>
  </div>
</section>
<?php endif; ?>