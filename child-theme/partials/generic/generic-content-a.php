<!-- Generic Content A -->
<?php 
$d_width = 1584;
$d_height = 450;
$swm_generic_content_background_image = get_sub_field('swm_generic_content_background_image');
$swm_generic_content_background_filter = get_sub_field('swm_generic_content_background_filter');
$swm_generic_content_background_color = get_sub_field('swm_generic_content_background_color');
$swm_generic_content_title = get_sub_field('swm_generic_content_title');
$swm_generic_content_banner_content = get_sub_field('swm_generic_content_banner_content');
$swm_generic_content_class = get_sub_field('swm_generic_content_class');
$get_background_image = swm_img_resize_url($d_width,$d_height,$swm_generic_content_background_image) ;
?>
<section class="generic-content-a generic-content-module current-theme banner-fade-background <?php echo $swm_generic_content_class; ?>" <?php echo swm_section_module_bg($swm_generic_content_background_color,$get_background_image); ?>>
	<div class="section-box load-filter position-relative">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="mod-content-fullwidth">
						<?php
							echo ($swm_generic_content_title) ? '<h3>' . $swm_generic_content_title . '</h3>' : '' ;
							echo $swm_generic_content_banner_content;
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>