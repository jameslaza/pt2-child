<!-- BirdEye (B) -->
<?php 
// Page Module
$swm_birdeye_activate = get_sub_field('swm_birdeye_activate');
$swm_birdeye_background_color = get_sub_field('swm_birdeye_background_color');
$swm_birdeye_header = get_sub_field('swm_birdeye_header');
$swm_birdeye_sub_text = get_sub_field('swm_birdeye_sub_text');

// PT Options
$swm_options_birdeye_background_color = get_field('swm_options_birdeye_background_color', 'option');
$swm_options_birdeye_header = get_field('swm_options_birdeye_header', 'option');
$swm_options_birdeye_sub_text = get_field('swm_options_birdeye_sub_text', 'option');
$swm_options_birdeye_code = get_field('swm_options_birdeye_code', 'option');

if($swm_birdeye_activate == false && $swm_birdeye_background_color !='') :
	$birdeye_background = $swm_birdeye_background_color;
else :
	$birdeye_background = $swm_options_birdeye_background_color;
endif;
?>


<?php  if($swm_options_birdeye_code) { ?>
<section class="birdeye-b birdeye dark-palette current-theme" <?php echo swm_section_module_bg($birdeye_background); ?>>
	<div class="section-box">
		<div class="container">
			<div class="row position-relative">
				<div class="col-md-8 horizontal-center">
					<span class="text-right section-title"><?php echo swm_target_module($swm_birdeye_header, $swm_options_birdeye_header, $swm_birdeye_activate); ?></span>
					<div class="text-right">
						<p><?php echo swm_target_module($swm_birdeye_sub_text, $swm_options_birdeye_sub_text, $swm_birdeye_activate); ?></p>
					</div>
				</div>
				<div class="col-md-4 pull-right birdeye-script">
		            <?php echo $swm_options_birdeye_code; ?>
	        	</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>