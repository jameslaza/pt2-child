<!-- All Services -->
<?php 
$swm_all_services_title = get_sub_field('swm_all_services_title');
$swm_all_services_background_color = get_sub_field('swm_all_services_background_color');
$swm_options_multiple_location_services_branch = get_field('swm_options_multiple_location_services_branch','option');
?>
<section class="all-services dark-palette" <?php echo swm_section_module_bg($swm_all_services_background_color); ?>>
	<div class="section-box">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php
						if($swm_options_multiple_location_services_branch) : echo ""; else : echo ($swm_all_services_title) ? '<span class="section-title">' . $swm_all_services_title . '</span>' : ''; endif;
						if ( has_nav_menu( 'services' ) ) {
						wp_nav_menu( array( 'theme_location' => 'services', 'menu_id' => 'service-names', 'menu_class' => 'service-names', 'link_before'=> '<i class="fa fa-chevron-circle-right"></i>') ); ?>
						<?php } else { ?>
							
							<?php echo swm_multiple_location_services(); ?>

						<?php }	?>
						<?php wp_reset_query(); ?>
				</div>
			</div>
		</div>
	</div>
</section>