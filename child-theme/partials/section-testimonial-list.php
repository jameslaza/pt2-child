<!-- Video -->
<?php
$swm_options_testimonial_item_thumb_width = get_field('swm_options_testimonial_item_thumb_width','option');
$swm_options_testimonial_item_thumb_height = get_field('swm_options_testimonial_item_thumb_height','option');
$swm_options_testimonial_play_icon = get_field('swm_options_testimonial_play_icon','option');
$swm_options_testimonial_item_load_more_items = get_field('swm_options_testimonial_item_load_more_items','option');
$swm_options_testimonial_item_excerpt = get_field('swm_options_testimonial_item_excerpt','option');
$swm_options_testimonial_transcript_button = get_field('swm_options_testimonial_transcript_button','option');

$birdeye_scroll_code = get_field('swm_options_birdeye_testimonial_code','option');
?>
<?php get_template_part('partials/section','testimonial-video'); ?>
<?php if($birdeye_scroll_code) : ?>
<section class="birdeye-section generic-main-content birdeye-wrap">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="entry-content">
          <?php echo do_shortcode('[birdeye_scroll_review]'); ?>
        </div>
      </div>
    </div>
  </div>
</section>
<?php endif; ?>

<?php /*
<!-- List Testimonial with Load More -->
<div class="testimonial-video-main-container">
<div class="container">
  <div class="row">
    <div class="col-md-12">

      <article>
        <div class="entry-content">
          <div class="testimonial-container">
            <?php
            $swm_testimonial_attr = swm_testimonial_attr();


            $excerpt_btn_text = 'Read More';
            $swm_cpt = swm_set_cpt();
            $testimonial_cpt = $swm_cpt['testimonial']['slug'];

            $thumb_w = $swm_options_testimonial_item_thumb_width;
            $thumb_h = $swm_options_testimonial_item_thumb_height;

            $excerpt_count =  $swm_options_testimonial_item_excerpt;

            if(! $excerpt_limit) : $excerpt_limit = $excerpt_count; endif;

            $args = array(
              'post_type' => 'testimonial',
              'meta_query' => array(
                array(
                  'key' => 'no_content',
                  'value' => '0',
                  'compare' => '=='
                )
              ),
              'order' => 'DESC',
              'orderby' => 'date',
              'posts_per_page' => $swm_testimonial_attr['posts_per_page'],
              'paged' => 1
            );

            $the_query = new WP_Query( $args );

            // The Loop
            if ( $the_query->have_posts() ) : ?>

            <div class="list-testimonial-section">

              <?php
              $maxpages = $the_query->max_num_pages;

              $ctr = 1;
              ?>

              <?php //if( get_the_content() != '') : ?>

              <div class="testimonial-video testimonial-video-text">

                <?php
                while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                <?php //if( get_the_content() != '') : ?>

                <div class="text-video-container">
                  <?php
                  $vid_src = get_field('select_video_source');
                  $youtubeid = get_field('testimonial_video');
                  $vimeoid = get_field('vimeo');

                  $crop_position = get_field('swm_crop_position',$id);

                  if ($vid_src == 'Youtube' && $youtubeid) :
                  $has_video = $youtubeid;
                  $href = '//www.youtube.com/watch?v=' . $has_video;
                  $img_video = swm_youtube_popup_post_thumb($youtubeid,$thumb_w,$thumb_h,'',$crop_position);
                  elseif ($vid_src == 'Vimeo' && $vimeoid) :
                  $has_video = $vimeoid;
                  $href = '//vimeo.com/' . $has_video;
                  $img_video = swm_vimeo_popup_post_thumb($vimeoid,$thumb_w,$thumb_h,'',$crop_position);
                  else :
                  $has_video = '';
                  endif;

                  ?>


                  <?php if ($has_video) : ?>
                  <div class="item">
                    <a href="<?php echo $href; ?>" class="popup-video transition">
                      <?php
                      if ( has_post_thumbnail() ) :
                      echo '<i class="fa fa-play-circle play-icon"></i>';
                      echo swm_img_resize("", $thumb_w,$thumb_h,$crop_position);
                      else :
                      echo $img_video;
                      endif;
                      ?>
                    </a>
                  </div>
                  <blockquote>
                    <span class="testi-title"><h3><?php echo get_the_title(); ?></h3></span>
                    <?php echo swm_customize_excerpt($excerpt_count,$excerpt_btn_text); ?>
                  </blockquote>
                  <div class="cf"></div>

                  <?php elseif ( has_post_thumbnail() ) : ?>

                  <div class="item">
                    <?php echo swm_img_resize("", $thumb_w,$thumb_h,$crop_position); ?>
                  </div>
                  <blockquote>
                    <span class="testi-title"><h3><?php echo get_the_title(); ?></h3></span>
                    <?php echo swm_customize_excerpt($excerpt_count,$excerpt_btn_text); ?>
                  </blockquote>


                  <div class="cf"></div>

                  <?php
                  else:
                  ?>
                  <blockquote class="full-width">
                    <span class="testi-title"><h3><?php echo get_the_title(); ?></h3></span>
                    <?php echo swm_customize_excerpt($excerpt_count,$excerpt_btn_text); ?>
                  </blockquote>

                  <?php endif; ?>

                </div>

                <?php //endif; ?>
                <?php
                endwhile; ?>


              </div>
            </div>
            <?php
            $count_post = $the_query->found_posts;
            echo swm_load_more_button($count_post);
            endif;

            // Reset Post Data
            wp_reset_postdata();
            wp_reset_query();
            swm_generate_thumb($thumb_w,$thumb_h);
            ?>
          </div>
        </div>
      </article>

    </div>
  </div>
</div>
</div>
<script>
  jQuery(document).ready(function($){

    $( document ).on( "click", "a.popup-video", function(e) {

      $(this).magnificPopup({
        disableOn: 0,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false,
        iframe: {
          markup: '<div class="mfp-iframe-scaler">'+
          '<div class="mfp-close"></div>'+
          '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
          '</div>', // HTML markup of popup, `mfp-close` will be replaced by the close button

          patterns: {
            youtube: {
              index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).

              id: 'v=', // String that splits URL in a two parts, second part should be %id%
              // Or null - full URL will be returned
              // Or a function that should return %id%, for example:
              // id: function(url) { return 'parsed id'; }

              src: '//www.youtube.com/embed/%id%?rel=0&autoplay=1&controls=0&showinfo=0&wmode=transparent' // URL that will be set as a source for iframe.
            },
            vimeo: {
              index: 'vimeo.com/',
              id: '/',
              src: '//player.vimeo.com/video/%id%?autoplay=1'
            }
            // you may add here more sources

          },

          srcAction: 'iframe_src', // Templating object key. First part defines CSS selector, second attribute. "iframe_src" means: find "iframe" and set attribute "src".
        }
      });
      e.preventDefault();
    });
  });
</script> */ ?>