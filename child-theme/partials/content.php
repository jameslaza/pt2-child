<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package swm
 */

$swm_cpt = swm_set_cpt();

// $doctors_cpt = $swm_cpt['doctors']['slug'];
// $staff_cpt = $swm_cpt['staffs']['slug'];
$doctors_cpt = 'doctor';
$staff_cpt = 'staff';
$excerpt_limit = get_field('swm_options_search_blog_excerpt_limit','option');

if (is_singular($doctors_cpt) || is_singular($staff_cpt)) { $addClass[] = "single-team"; }

?>

<article id="post-<?php the_ID(); ?>" <?php post_class($addClass); ?>>
  <header class="entry-header has-subtitle">
    <?php
      if ( is_single() ) { ?>

        <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

            <?php if (get_field('sub_title')) : ?>
                <h2><?php the_field('sub_title'); ?></h2>
            <?php endif;

          if (is_singular($doctors_cpt)) {
              echo get_doctor_location();
          }elseif (is_singular($staff_cpt)){
              echo get_staff_position_location();
          }

      } else {
        the_title( '<span class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></span>' );
      }

    if ( 'post' === get_post_type() || 'news' === get_post_type() ) : ?>
    <div class="entry-meta">
      <span class="posted-on">
        <time class="entry-date published"><i class="fa fa-calendar"></i> <?php the_time('F j, Y');?></time>
      </span>
    </div><!-- .entry-meta -->
    <?php
    endif; ?>
  </header><!-- .entry-header -->

  <div class="entry-content">

    <?php
      
      if ( is_single() ) {

        if (is_singular($doctors_cpt) || is_singular($staff_cpt)) {
          echo get_team_photo();
        }else {
          echo swm_img_resize('',250,200,'alignleft');
        }

        the_content();

      }else {

        echo swm_excerpt(($excerpt_limit)? : '250');

      }

      wp_link_pages( array(
        'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'swm' ),
        'after'  => '</div>',
      ) );

    ?>
  </div><!-- .entry-content -->
  <div class="cf"></div>
  <footer class="entry-footer">
    <?php //swm_entry_footer(); ?>
  </footer><!-- .entry-footer -->

</article><!-- #post-## -->
<div class="cf"></div>
