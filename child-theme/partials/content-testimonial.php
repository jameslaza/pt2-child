<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package swm
 */
global $post;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		 	<header class="entry-header has-subtitle">
		            <?php //swm_breadcrumbs();?>

				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		        <?php if (get_field('sub_title')) : ?>
		            <h2><?php the_field('sub_title'); ?></h2>
		        <?php endif; ?>
			</header><!-- .entry-header -->

			<div class="entry-content single-testimonial-content">
				<?php
				$swm_options_testimonial_item_thumb_width = get_field('swm_options_testimonial_item_thumb_width','option');
				$swm_options_testimonial_item_thumb_height = get_field('swm_options_testimonial_item_thumb_height','option');
				$vid_src = get_field('select_video_source');

				$youtubeid = get_field('testimonial_video');
				$vimeoid = get_field('vimeo');
				$crop = get_field('swm_crop_position');
      			if(! $crop): $crop = 'c'; endif;

				$addClass = 'alignleft';


			if( '' !== get_post()->post_content ) {
				$thumb_w = $swm_options_testimonial_item_thumb_width;
				$thumb_h = $swm_options_testimonial_item_thumb_height;
				$addClass = "";
			}else{
				$thumb_w = 350;
				$thumb_h = 200;
				$addClass = " video-only";
			}
				

				if ($vid_src == 'Youtube' && $youtubeid) : ?>
						<div class="testimonial-video testimonial-video-text">
							<div class="text-video-container">
								<div class="item<?php echo $addClass; ?>">
									<?php echo swm_youtube_popup_post_thumb($youtubeid,$thumb_w,$thumb_h,$addClass,$crop); ?>
								</div>

				               	<blockquote>
				                	<?php the_content(); ?>
				                </blockquote>

				                <div class="cf"></div>
							</div>
		        		</div>

				<?php elseif ($vid_src == 'Vimeo' && $vimeoid) : ?>
						<div class="testimonial-video testimonial-video-text">
							<div class="text-video-container">
								<div class="item">
									<?php echo swm_vimeo_popup_post_thumb($vimeoid,$thumb_w,$thumb_h,$addClass,$crop); ?>
								</div>

				               	<blockquote>
				                	<?php the_content(); ?>
				                </blockquote>

				                <div class="cf"></div>
							</div>
		        		</div>

				<?php else:

					if ( has_post_thumbnail($id) ) : ?>
						<div class="testimonial-video testimonial-video-text">
							<div class="text-video-container">
								<div class="item">
									<?php echo swm_img_resize('',$thumb_w,$thumb_h,$crop_position,'testimonial-img'); ?>
								</div>

				               	<blockquote>
				                	<?php the_content(); ?>
				                </blockquote>

				                <div class="cf"></div>
							</div>
		        		</div>
					<?php
					else :
						the_content();
					endif;

				endif;

				?>
			</div><!-- .entry-content -->

			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'swm' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>

</article><!-- #post-## -->
