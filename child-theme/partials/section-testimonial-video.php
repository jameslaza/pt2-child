<!-- Testimonial Videos -->
<?php
$swm_options_testimonial_item_thumb_width = get_field('swm_options_testimonial_item_thumb_width','option');
$swm_options_testimonial_item_thumb_height = get_field('swm_options_testimonial_item_thumb_height','option');
/*$swm_options_testimonial_item_thumb_width = 350;
$swm_options_testimonial_item_thumb_height = 200;*/
$swm_options_testimonial_play_icon = get_field('swm_options_testimonial_play_icon','option');
$swm_options_testimonial_item_load_more_items = get_field('swm_options_testimonial_item_load_more_items','option');
$swm_options_testimonial_item_excerpt = get_field('swm_options_testimonial_item_excerpt','option');
$swm_options_testimonial_transcript_button = get_field('swm_options_testimonial_transcript_button','option');?>


<?php
$args = array(
  'post_type' => 'testimonial',
  'posts_per_page' => -1,
  'meta_query' => array(
    'relation' => 'AND',
    array(
      'key' => 'select_video_source',
      'value' => 'no_video',
      'compare' => '!='
    ),
    array(
      'key' => 'no_content',
      'value' => '1',
      'compare' => '=='
    )
  ),
  'order'          => 'DESC',
  'orderby'        => 'date',
);

$the_query = new WP_Query( $args );

$thumb_w = $swm_options_testimonial_item_thumb_width;
$thumb_h = $swm_options_testimonial_item_thumb_height;

if ( $the_query->have_posts() ) : ?>

<div class="video-main-container">
  <div class="container">
    <h3>Hear What Our Patients Are Saying</h3>
    <div class="row">
      <div class="col-md-12">
        <div class="entry-content">

        <div class="testimonial-video">
          <?php
          while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

          <?php //if( get_the_content() == '') : ?>

          <?php $vid_src = get_field('select_video_source'); ?>

          <?php $youtubeid = get_field('testimonial_video'); ?>
          <?php $vimeoid = get_field('vimeo'); ?>
          <?php
          $crop = get_field('swm_crop_position');
          if(! $crop): $crop = 'c'; endif;
          ?>

          <?php $crop_position = get_field('crop_alignment',$id); ?>

          <?php
          if ($vid_src == 'Youtube' && $youtubeid) :
          $has_video = $youtubeid;
          $href = 'http://www.youtube.com/watch?v=' . $has_video;
          $img_video = swm_youtube_thumb($youtubeid,$thumb_w,$thumb_h,'',$crop);
          elseif ($vid_src == 'Vimeo' && $vimeoid) :
          $has_video = $vimeoid;
          $href = 'https://vimeo.com/' . $has_video;
          $img_video = swm_vimeo_thumb($vimeoid,$thumb_w,$thumb_h,'',$crop);
          else :
          $has_video = '';
          endif;
          ?>
       <?php $transcript = get_field('swm_testimonial_transcript');
             $transcript_class = ($transcript!='')? 'transcript-wrapper':'';
           ?>
          <?php if ($has_video) : ?>

          <div class="item <?php echo $transcript_class; ?> patient-testimonial-video-list">
            <a href="<?php echo $href; ?>" class="popup-video transition">
              <i class="fa <?php if($swm_options_testimonial_play_icon) : echo $swm_options_testimonial_play_icon; else : echo 'fa-play'; endif; ?> play-icon"></i>
              <span class="testimonial-title">
                <span class="tbl">
                  <span class="tbl-cell">
                    <?php the_title(); ?>
                  </span>
                </span>
              </span>

              <?php
              if ( has_post_thumbnail() ) :
              echo swm_img_resize("",$thumb_w,$thumb_h,$crop_position);
              else :
              echo $img_video;
              endif;
              ?>

            </a>

            <?php if($transcript) :?>
            <?php $rand = rand(5, 115); ?>
            <a href="#popup-<?php echo $rand; ?>" class="transcript-link transcript-btn open-popup-link transition"><span class="read-transcript"><?php if($swm_options_testimonial_transcript_button ) : echo $swm_options_testimonial_transcript_button ; else : echo 'Read Transcript'; endif; ?></span></a>
            <?php endif; ?>

            <?php if($transcript) :?>
            <div class="popup-content mfp-hide" id="popup-<?php echo $rand; ?>">
              <p><?php echo $transcript; ?></p>
            </div>
            <?php endif; ?>
          </div>

          <?php endif; ?>

          <?php //endif; ?>

          <?php
          endwhile; ?>



          <?php //echo do_shortcode('[birdeye_scroll_review]'); ?>

        </div>

        </div>
      </div>
    </div>
  </div>
</div>




<?php
wp_reset_postdata();
endif;
wp_reset_query(); ?>