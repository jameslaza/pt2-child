<!-- Doctor Overview D -->
<?php 

$width = 308;
$height = 308;

$swm_doctors_activate_read_more = get_sub_field('swm_doctors_activate_read_more');
// Overview D
$swm_doctors_top_button = get_sub_field('swm_doctors_top_button');
$swm_doctors_top_button_link = get_sub_field('swm_doctors_top_button_link');
$swm_doctors_middle_button = get_sub_field('swm_doctors_middle_button');
$swm_doctors_middle_button_link = get_sub_field('swm_doctors_middle_button_link');
$swm_doctors_bottom_button = get_sub_field('swm_doctors_bottom_button');
$swm_doctors_bottom_button_link = get_sub_field('swm_doctors_bottom_button_link');
$swm_doctorssection_title = get_sub_field('swm_doctors_section_title');


if( have_rows('swm_doctors_overview') ): ?>
<section class="doctor-overview light-palette current-theme" <?php echo swm_section_module_bg($swm_doctors_background_color); ?>>
	<div class="section-box">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php if($swm_doctorssection_title){ ?>
						<h1><?php echo $swm_doctorssection_title; ?></h1>
					<?php } ?>
					<div class="circle-container swm-col-3">
						<div class="list-item-container">
						    <?php
						    while ( have_rows('swm_doctors_overview') ) : the_row();

						    	$swm_doctors_get_doctor = get_sub_field('swm_doctors_get_doctor');
						    	$swm_doctors_target_doctor = get_sub_field('swm_doctors_target_doctor');

								// CPT Fields
								$get_doctor_url = swm_obj($swm_doctors_target_doctor,'url');
								$doctor_id = swm_obj($swm_doctors_target_doctor,'id');
								$swm_doctors_background_color = get_sub_field('swm_doctors_background_color');

						    	if($swm_doctors_get_doctor == 'CPT') : 
									$get_doctor_name = swm_obj($swm_doctors_target_doctor,'title');
									//$get_content = swm_obj($swm_doctors_target_doctor,'content');
									$get_content = get_field('swm_doctor_intro_content',$doctor_id);
									$swm_doctor_intro_title = get_field('swm_doctor_intro_title', $doctor_id);
									$get_video = get_field('video', $doctor_id);
									$swm_doctors_doctor_photo = get_field('photo', $doctor_id);
									// CPT Links
									$swm_doctor_left_label = get_field('swm_doctor_left_label', $doctor_id);
									$swm_doctor_left_link = get_field('swm_doctor_left_link', $doctor_id);
									$swm_doctor_right_label = get_field('swm_doctor_right_label', $doctor_id);
									$swm_doctor_right_link = get_field('swm_doctor_right_link', $doctor_id);
								else :
									$get_doctor_name = get_sub_field('swm_doctors_name');
									$get_content = get_sub_field('swm_doctors_content');
									$swm_doctor_intro_title = get_sub_field('swm_doctors_name');
									$swm_doctors_doctor_photo = get_sub_field('swm_doctors_doctor_photo');
									$get_video = get_sub_field('swm_doctors_youtube_video');
									// Custom Links
									$swm_doctors_left_label = get_sub_field('swm_doctors_left_label');
									$swm_doctors_left_link = get_sub_field('swm_doctors_left_link');
									$swm_doctors_right_label = get_sub_field('swm_doctors_right_label');
									$swm_doctors_right_link = get_sub_field('swm_doctors_right_link');
								endif;

								if($get_video) : 
									$photo_class = '';
									$content_class = 'col-md-7';
									$get_photo = ($swm_doctors_doctor_photo) ? swm_img_resize_obj($width,$height,$swm_doctors_doctor_photo) : swm_youtube_thumb($get_video,$width,$height) ;
								else :
									$photo_class = ($swm_doctors_doctor_photo) ? '' : 'hidden';
									$content_class = ($swm_doctors_doctor_photo) ? 'col-md-7' : 'col-md-12' ;
									$get_photo = ($swm_doctors_doctor_photo) ? swm_img_resize_obj($width,$height,$swm_doctors_doctor_photo) : '' ;
								endif;

								$content = ($swm_doctors_activate_read_more == true) ? swm_custom_excerpt($get_content,200,$get_doctor_url) : wpautop($get_content) ;
						    ?>

							<div class="list-item">
								<?php if($get_photo && $swm_doctors_target_doctor) : ?>
								<a class="transition" href="<?php echo $get_doctor_url; ?>">
									<span class="list-item-img">
										<?php echo $get_photo; ?>
									</span>
								</a>
								<?php else : ?>
								<span class="list-item-img">
									<?php echo $get_photo; ?>
								</span>
								<?php endif; ?>
								<div class="item-title-container">
									<?php if($swm_doctors_target_doctor) : ?>
										<a class="transition" href="<?php echo $get_doctor_url; ?>"><span class="item-title"><?php echo $swm_doctor_intro_title; ?></span></a>
									<?php else : ?>
										<span class="item-title"><?php echo $swm_doctor_intro_title; ?></span>
									<?php endif; ?>
								</div>
								<div class="item-content-container">
									<div class="item-content">
										<?php echo $content; ?>
									</div>
								</div>
								<a class="transition" href="<?php echo $get_doctor_url; ?>">Meet <?php echo $swm_doctor_intro_title; ?> <i class="fa fa-chevron-circle-right"></i></a>
							</div>

							<?php endwhile; ?>

							<?php if($swm_doctors_top_button || $swm_doctors_middle_button || $swm_doctors_bottom_button) : ?>
							<div class="ask-doctor">
								<?php if($swm_doctors_top_button) : ?>
								<div><a href="<?php echo $swm_doctors_top_button_link; ?>" class="ask-the-doctor"><?php echo $swm_doctors_top_button; ?></a></div>
								<?php endif; ?>
								<?php if($swm_doctors_middle_button) : ?>
								<a href="<?php echo $swm_doctors_middle_button_link; ?>" class="btn-default-dark"><?php echo $swm_doctors_middle_button; ?></a>
								<?php endif; ?>
								<?php if($swm_doctors_bottom_button) : ?>
								<div><a href="<?php echo $swm_doctors_bottom_button_link; ?>" class="btn-text block-me"><?php echo $swm_doctors_bottom_button; ?></a></div>
								<?php endif; ?>
							</div>
							<?php endif; ?>
							<div class="cf"></div> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>